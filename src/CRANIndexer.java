import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.LMJelinekMercerSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class CRANIndexer {

	/**
	 * Clase que servira para almacenar los documentos Cranfield
	 * 
	 */
	private class CRANDocument {
		private String docID; // .I
		private String title; // .T
		private String authors; // .A
		private String abstractTitle; // .W

		public CRANDocument() {
			this.docID = "";
			this.title = "";
			this.authors = "";
			this.abstractTitle = "";

		}

		public String getDocID() {
			return docID;
		}

		public void setDocID(String docID) {
			this.docID = docID;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getAuthors() {
			return authors;
		}

		public void setAuthors(String authors) {
			this.authors = authors;
		}

		public String getAbstractTittle() {
			return abstractTitle;
		}

		public void setAbstractTittle(String abstractTittle) {
			this.abstractTitle = abstractTittle;
		}

		public void addAuthor(String author) {
			if (this.authors.isEmpty())
				this.authors += author;
			else
				this.authors += " " + authors;
		}

		public void addTitle(String title) {
			if (this.title.isEmpty())
				this.title += title;
			else
				this.title += " " + title;
		}

		public void addAbstractTitle(String abstractTitle) {
			if (this.abstractTitle.isEmpty())
				this.abstractTitle += abstractTitle;
			else
				this.abstractTitle += " " + abstractTitle;
		}

		@Override
		public String toString() {
			return "CRANDocument docID=" + docID + "\n title=" + title + "\n abstractTitle=" + abstractTitle
					+ "\n authors=" + authors;
		}

	}

	/**
	 * Clase que implementa el parser necesario para devolver una lista con los
	 * documentos CRAN
	 */
	private class CRANParser {

		BufferedReader buffer;
		List<CRANDocument> documentList;

		public CRANParser(String file) {
			this.documentList = new ArrayList<CRANDocument>();

			try {
				this.buffer = new BufferedReader(new FileReader(file));
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			}

		}

		public List<CRANDocument> parse() {
			String linea;
			char letraEstado = 0;
			CRANDocument document = null;

			try {
				while ((linea = buffer.readLine()) != null) {

					if ((linea = linea.trim()).isEmpty()) {
						continue;
					}

					if (linea.charAt(0) == '.') {
						letraEstado = linea.charAt(1);
						if (letraEstado == 'I') {

							if (document != null)
								this.documentList.add(document);

							document = new CRANDocument();
							document.setDocID(linea.substring(2).trim());
						}
					} else {
						switch (letraEstado) {
						case 'T': // Title
							document.addTitle(linea);
							break;
						case 'W': // Abstract
							document.addAbstractTitle(linea);
							break;
						case 'A': // Authors
							document.addAuthor(linea);
							break;

						default:
							break;
						}
					}

				}
			} catch (Exception e) {
				System.out.println("Excepcion parseando: " + e);
			}

			this.documentList.add(document);

			try {
				buffer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return this.documentList;

		}

	}



	public void indexDocuments(CRANEval.Parametros parameters) {

		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		IndexWriter writer = null;
		CRANParser parser = null;
		List<CRANDocument> resultado = new ArrayList<CRANDocument>();
		
		if (parameters.getIndexPath().equals(""))
			return;
		
		

		switch (parameters.getMode()) {

		case "append":
			iwc.setOpenMode(OpenMode.APPEND);
			break;
		case "create_or_append":
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			break;
		default:
			iwc.setOpenMode(OpenMode.CREATE);
			break;
		}

		if (!parameters.getCollPath().equals("")) {
			parser = new CRANParser(parameters.getCollPath());
			resultado = parser.parse();

		}
		
		if (!parameters.getIndexingMode().equals("")) {
			
			switch (parameters.getIndexingMode() ) {
	
			case "jm":
				System.out.println("ENTROOOOO: JM " );
				LMJelinekMercerSimilarity jmSimilarity = new LMJelinekMercerSimilarity(parameters.getLambdaOrMu());
				iwc.setSimilarity(jmSimilarity);
				break;
			case "dir":
				LMDirichletSimilarity dirSimilarity = new LMDirichletSimilarity(parameters.getLambdaOrMu());
				iwc.setSimilarity(dirSimilarity);
				break;
			default:
				System.out.println("ENTROOOOO: default " );
				break;
			}
		}

		if (!parameters.getCollPath().equals("")) {
			parser = new CRANParser(parameters.getCollPath());
			resultado = parser.parse();

		}		
		 

		if (!parameters.getIndexPath().equals("")) {

			try {
				Directory dir = FSDirectory.open(Paths.get(parameters.getIndexPath()));
				writer = new IndexWriter(dir, iwc);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		Field docIDField = new StringField("docid", "", Field.Store.YES);
	    Field titleField = new TextField("title", "", Field.Store.YES);
	    Field abstractTitleField = new TextField("abstract", "", Field.Store.YES);
	    Field authorsField = new TextField("authors", "", Field.Store.YES);

		for (CRANDocument d : resultado) {
			Document doc = new Document(); 
			
			docIDField.setStringValue(d.getDocID());
			doc.add(docIDField);
			
			titleField.setStringValue(d.getTitle());
	        doc.add(titleField);
	        
	        abstractTitleField.setStringValue(d.getAbstractTittle());
	        doc.add(abstractTitleField);
			
	        authorsField.setStringValue(d.getAuthors().toString());
	        doc.add(authorsField);
	        
	        try {
				writer.addDocument(doc);
			} catch (IOException e) {
				e.printStackTrace();
			}


		}

		try {
			writer.commit();
			writer.close();
			System.out.println("\nDocs indexed in " + parameters.getIndexPath());
		} catch (CorruptIndexException e) {
			System.out.println("Graceful message: exception " + e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Graceful message: exception " + e);
			e.printStackTrace();
		}

	}


}
