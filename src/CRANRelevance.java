import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.LeafReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.QueryParser;



public class CRANRelevance {

	/**
	 * Clase para guardar los terminos y sus DF asociados
	 *
	 */


	public class ResultIDF implements Comparable<ResultIDF> {

		private final String termino;
		private final int DF;
		private double IDF;

		public ResultIDF(String termino, int dF, int nDocs) {
			super();
			this.termino = termino;
			this.DF = dF;
			setIDF(nDocs);

		}

		public String getTermino() {
			return termino;
		}

		public int getDF() {
			return DF;
		}

		public double getIDF() {
			return IDF;
		}

		public void setIDF(int numDocs) {
			IDF = Math.log((double) numDocs / this.DF);
		}

		@Override
		public String toString() {
			return " [termino=" + termino + ", DF=" + DF + ", IDF=" + IDF + "]";
		}

		@Override
		public int compareTo(ResultIDF other) {

			if (this.IDF < other.IDF)
				return 1;
			else if (this.IDF > other.IDF)
				return -1;

			if (this.DF < other.DF)
				return 1;
			else if (this.DF > other.DF)
				return -1;

			return this.termino.compareTo(other.getTermino());

		}

	}

	/**
	 * Clase para guardar los resultados con su TF asociada
	 *
	 */

	public class ResultTFIDF implements Comparable<ResultTFIDF> {

		private final int docId;
		private final int TF;
		private double tfidf;
		private ResultIDF IDF;

		public ResultTFIDF(String termino, int dF, int tf, int docId, int nDocs) {
			this.IDF = new ResultIDF(termino, dF, nDocs);
			this.TF = tf;
			this.docId = docId;
			this.setTfidf();

		}

		public int getTF() {
			return TF;
		}

		@Override
		public String toString() {
			
			int docIndexed = docId +1;
			return " [doc=" + docIndexed + ", termino=" + this.IDF.getTermino() + ", TF=" + TF + ", DF=" + this.IDF.getDF()
					+ ", IDF=" + this.IDF.getIDF() + ", tfidf=" + tfidf + "]";

		}

		public int getDocId() {
			return docId;
		}

		public double getTfidf() {
			return tfidf;
		}

		public void setTfidf() {
			// this.tfidf = (1 + Math.log((double) this.getDF())) *
			// this.getTF();
			this.tfidf = this.TF * this.IDF.getIDF();
		}

		public String getTermino() {
			return IDF.termino;
		}

		public int getDF() {
			return IDF.DF;
		}

		public double getIDF() {
			return IDF.IDF;
		}

		@Override
		public int compareTo(ResultTFIDF other) {

			if (this.tfidf < other.tfidf)
				return 1;
			else if (this.tfidf > other.tfidf)
				return -1;

			if (this.getIDF() < other.getIDF())
				return 1;
			else if (this.getIDF() > other.getIDF())
				return -1;

			if (this.getDocId() < other.getDocId())
				return 1;
			else if (this.getDocId() > other.getDocId())
				return -1;

			return this.getTermino().compareTo(other.getTermino());

		}

	}
	
	public ArrayList<ResultTFIDF> processIDFTF(DirectoryReader reader, String queryField) throws IOException {

		// DirectoryReader reader2 = (DirectoryReader) reader;
		ArrayList<ResultTFIDF> resList = new ArrayList<ResultTFIDF>();

		for (final LeafReaderContext leaf : reader.leaves()) {
			// Print leaf number (starting from zero)

			// Create an AtomicReader for each leaf
			// (using, again, Java 7 try-with-resources syntax)
			try (LeafReader leafReader = leaf.reader()) {

				// Get the fields contained in the current segment/leaf
				final Fields fields = leafReader.fields();

				for (final String field : fields) {

					if (field.equals(queryField)) {

						final Terms terms = fields.terms(field);
						final TermsEnum termsEnum = terms.iterator();

						while (termsEnum.next() != null) {

							Term term = new Term(queryField, termsEnum.term());
							PostingsEnum postingsEnum = leafReader.postings(term);

							int doc;

							String tt = termsEnum.term().utf8ToString();
							int df = termsEnum.docFreq();

							while ((doc = postingsEnum.nextDoc()) != PostingsEnum.NO_MORE_DOCS) {

								int tf = postingsEnum.freq();
								int docId = postingsEnum.docID();
							

								resList.add(new ResultTFIDF(tt, df, tf, docId, reader.numDocs()));

							}

						}

					}

				}

			}

		}

		Collections.sort(resList);

		return resList;

	}

	public ArrayList<ResultIDF> processIDFQuery(IndexReader reader, String query, String queryField, int nTerminos) {

		int numDocs = reader.numDocs();
		StringTokenizer st;

		ArrayList<ResultIDF> resList = new ArrayList<ResultIDF>(); 

		st = new StringTokenizer(QueryParser.escape(query), " ");

		while (st.hasMoreElements()) {
			String term = st.nextToken();

			int df = 0;

			try {
				df = reader.docFreq(new Term(queryField, term));
			} catch (IOException e) {

				e.printStackTrace();
			}

			ResultIDF tupla = new ResultIDF(term, df, numDocs);

			if (df > 0)
				resList.add(tupla);

		}

		Collections.sort(resList);

		return resList;

	}

	private ArrayList<ResultTFIDF> filtrarDocId(ArrayList<ResultTFIDF> lista, ArrayList<Integer> docList) {

		ArrayList<ResultTFIDF> resultados = new ArrayList<>();

		for (Integer i : docList) {
			for (ResultTFIDF t : lista) {
				
				if (i == 0)
					System.out.println("DOCID ES FALSO");
				
				if (t.docId+1 == i) {
					resultados.add(t);
				}

			}

		}
		return resultados;
	}

	public ArrayList<String> mejoresTermsDoc(ArrayList<ResultTFIDF> resultadosTFIDF,
			ArrayList<Integer> docList, int nDocuments, int numeroTerminos, boolean explain, ArrayList<String> explicacion) {

		ArrayList<String> terminosSeleccionados = new ArrayList<>();
		ArrayList<Integer> relevantes = new ArrayList<>();

		
		//System.out.println(nDocuments);
		for (int k = 0; k < nDocuments; k++) {
		
			relevantes.add(docList.get(k));
		}

		ArrayList<ResultTFIDF> terminos = this.filtrarDocId(resultadosTFIDF, relevantes);

		Collections.sort(terminos);
		
		
		
		
		for (int j=0; j < numeroTerminos; j++)
		{
			if (explain) {
				//System.out.println("\tResultTFIDF= " + terminos.get(j));
				explicacion.add(new String("\tResultTFIDF= " + terminos.get(j)));
			}	
				terminosSeleccionados.add(terminos.get(j).getTermino());
			
		}

		return terminosSeleccionados;
	}

}
