import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.LMJelinekMercerSimilarity;
import org.apache.lucene.store.FSDirectory;

/*
 * Para probar esta parte Fernando:
 * -indexin /Users/javi/Documents/workspace/jri-searcher/index  -cut 10 -queries 3  7  -fieldsproc T,W  -fieldsvisual T  -top 10 -search default -rf1 4 4  5 -explain
 * 
 * 
 * 
 */

public class CRANEval {

	private enum ClaseConsulta {
		QUERIES, RF1, RF2, RF3
	}

	private int numberOfQueriesEvaluated;
	private float totalR10, totalR20, totalP10, totalP20, totalMAP, totalR1;

	public CRANEval() {
		this.totalP10 = 0f;
		this.totalR10 = 0f;
		this.totalP20 = 0f;
		this.totalR20 = 0f;
		this.totalMAP = 0f;
		this.totalR1 = 0f;

	}

	public void clearEval() {

		this.numberOfQueriesEvaluated = 0;
		this.totalP10 = 0f;
		this.totalR10 = 0f;
		this.totalP20 = 0f;
		this.totalR20 = 0f;
		this.totalMAP = 0f;
		this.totalR1 = 0f;

	}

	public void setP10(float amount) {
		this.totalP10 += amount;

	}

	public void setR10(float amount) {
		this.totalR10 += amount;
	}

	public void setP20(float amount) {
		this.totalP20 += amount;
	}

	public void setR20(float amount) {
		this.totalR20 += amount;
	}
	public void setR1(float amount) {
		this.totalR1 += amount;
	}
	

	public void setMAP(float amount) {
		this.totalMAP += amount;
	}

	public void addEvaluated() {
		this.numberOfQueriesEvaluated++;
	}

	public int getNumberOfQueriesEvaluated() {
		return numberOfQueriesEvaluated;
	}

	public void setNumberOfQueriesEvaluated(int numberOfQueriesEvaluated) {
		this.numberOfQueriesEvaluated = numberOfQueriesEvaluated;
	}

	public float getTotalR10() {
		return totalR10;
	}

	public float getTotalR20() {
		return totalR20;
	}

	public float getTotalP10() {
		return totalP10;
	}

	public float getTotalP20() {
		return totalP20;
	}

	public float getTotalMAP() {
		return totalMAP;
	}

	private void showResults() {

		System.out.println("\n\nDe " + this.numberOfQueriesEvaluated + " queries analizadas:");

		System.out.println("P@10: " + this.totalP10 / this.numberOfQueriesEvaluated);
		System.out.println("P@20: " + this.totalP20 / this.numberOfQueriesEvaluated);
		System.out.println("R@10: " + this.totalR10 / this.numberOfQueriesEvaluated);
		System.out.println("R@20: " + this.totalR20 / this.numberOfQueriesEvaluated);
		System.out.println("MAP: " + this.totalMAP / this.numberOfQueriesEvaluated);
		System.out.println("R1: " + this.totalR1 / this.numberOfQueriesEvaluated);

	}

	/**
	 * Clase donde se guarda la info de cada query
	 * 
	 * @author javi
	 *
	 */
	private class CRANQuery {

		private int id;
		private String qText_sinModificar;
		private String qText;
		private ArrayList<String> explicacion;
		Query query = null;
		TopDocs results = null;
		QRel rel = null;
		float p10, p20, r10, r20, map, r1;

		public CRANQuery() {
			this.qText_sinModificar = "";
			this.explicacion = null;
			this.qText = "";
			this.id = -1;
			this.p10 = -1;
			this.p20 = -1;
			this.r10 = -1;
			this.r20 = -1;
			this.map = -1;
			this.r1 = -1;
		}

		public int getId() {
			return id;
		}

		public void addText(String qText) {
			this.qText += (this.qText.isEmpty() ? "" : " ") + qText;
		}

		public String getqText() {
			return qText;
		}

		public void setqRext(String queryModificada) {
			this.qText = queryModificada;

		}

		public void setqRext_sinModificar(String query) {
			this.qText_sinModificar = query;

		}

		public void setExplicacion(ArrayList<String> exp) {
			this.explicacion = new ArrayList<String>();
			this.explicacion = exp;

		}

		public Query getQuery() {
			return query;
		}

		public QRel getRel() {
			return rel;
		}

		public void setQrel(QRel rel) {
			this.rel = rel;
		}

		/* Procesa todas las metricas */
		public void proMetricas(IndexSearcher searcher, Parametros parametros, CRANEval eval) {
			if (this.rel != null) {
				eval.setP10(this.calcularP10(searcher));
				eval.setP20(this.calcularP20(searcher));
				eval.setR10(this.calcularR10(searcher));
				eval.setR20(this.calcularR20(searcher));
				eval.setMAP(this.calcularMAP(searcher, parametros.cutN));
				eval.setR1(this.calcularR1(searcher));
				eval.addEvaluated();
			}
		}

		private float calcularMAP(IndexSearcher searcher, int cutN) {
			Document doc = null;
			ScoreDoc[] hits = this.results.scoreDocs;
			int numTotalHits = this.results.totalHits;
			ArrayList<String> recs = new ArrayList<String>();
			float partialMAP = 0f;
			int ndr = 0;

			for (int i = 0; i < Math.min(cutN, numTotalHits); i++) {
				try {
					doc = searcher.doc(hits[i].doc);
				} catch (IOException e) {

					e.printStackTrace();
				}
				String docId = doc.get("docid");
				recs.add(docId);
				if (this.rel.esRelevante(Integer.parseInt(docId))) {
					ndr++;
					partialMAP += (float) ndr / (i + 1);
				}
			}

			this.map = (float) partialMAP / rel.getDocList().size();

			return this.map;
		}

		private float calcularR20(IndexSearcher searcher) {
			Document doc = null;
			ScoreDoc[] hits = this.results.scoreDocs;
			int numTotalHits = this.results.totalHits;
			int ndr = 0;
			ArrayList<String> recs = new ArrayList<String>();

			for (int i = 0; i < Math.min(20, numTotalHits); i++) {
				try {
					doc = searcher.doc(hits[i].doc);
				} catch (IOException e) {

					e.printStackTrace();
				}
				String docId = doc.get("docid");
				recs.add(docId);
				if (this.rel.esRelevante(Integer.parseInt(docId))) {
					ndr++;

				}
			}

			this.r20 = (float) ndr / this.rel.getDocList().size();
			return this.r20;

		}

		private float calcularR10(IndexSearcher searcher) {
			Document doc = null;
			ScoreDoc[] hits = this.results.scoreDocs;
			int numTotalHits = this.results.totalHits;
			int ndr = 0;
			ArrayList<String> recs = new ArrayList<String>();

			for (int i = 0; i < Math.min(10, numTotalHits); i++) {
				try {
					doc = searcher.doc(hits[i].doc);
				} catch (IOException e) {
					//
					e.printStackTrace();
				}
				String docId = doc.get("docid");
				recs.add(docId);
				if (this.rel.esRelevante(Integer.parseInt(docId))) {
					ndr++;
				}
			}
			this.r10 = (float) ndr / this.rel.getDocList().size();
			return this.r10;
		}

		private float calcularP20(IndexSearcher searcher) {

			Document doc = null;
			ScoreDoc[] hits = this.results.scoreDocs;
			int numTotalHits = this.results.totalHits;
			ArrayList<String> recs = new ArrayList<String>();
			int rec_rel = 0;
			for (int i = 0; i < Math.min(20, numTotalHits); i++) {
				try {
					doc = searcher.doc(hits[i].doc);
				} catch (IOException e) {

					e.printStackTrace();
				}
				String docId = doc.get("docid");
				recs.add(docId);
				if (this.rel.esRelevante(Integer.parseInt(docId))) {
					rec_rel++;
				}
			}

			this.p20 = rec_rel / 20f;

			return this.p20;
		}

		private float calcularP10(IndexSearcher searcher) {
			Document doc = null;
			ScoreDoc[] hits = this.results.scoreDocs;
			int numTotalHits = this.results.totalHits;
			int rec_rel = 0;
			ArrayList<String> recs = new ArrayList<String>();

			for (int i = 0; i < Math.min(10, numTotalHits); i++) {
				try {
					doc = searcher.doc(hits[i].doc);
				} catch (IOException e) {

					e.printStackTrace();
				}
				String docId = doc.get("docid");
				recs.add(docId);
				if (this.rel.esRelevante(Integer.parseInt(docId))) {
					rec_rel++;

				}
			}

			this.p10 = rec_rel / 10f;
			return this.p10;
		}

		
		private float calcularR1 (IndexSearcher searcher) {
			Document doc = null;
			
			int numTotalHits = this.results.totalHits;
			int rec_rel = 0;
			ArrayList<String> recs = new ArrayList<String>();
			float ultimoRelevante = 0;
			float numRelevantes = 0;
			
			TopDocs results = null;
			
			try {
				results = searcher.search(query,  numTotalHits);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			ScoreDoc[] hits = results.scoreDocs;

			for (int i = 0; i <  Math.min(hits.length, results.totalHits); i++) {
				try {
					doc = searcher.doc(hits[i].doc);
				} catch (IOException e) {

					e.printStackTrace();
				}
				String docId = doc.get("docid");
				recs.add(docId);
				
				if (this.rel.esRelevante(Integer.parseInt(docId))) {
					ultimoRelevante = i;
					numRelevantes++;

				}
			}
			
			//System.out.println("hits numero  = " + hits.length + " ultimoRelevante = " + ultimoRelevante + "numRelevantes = " + numRelevantes + "num rel lista = " + rel.docList.size());

			this.r1 =  numRelevantes / (ultimoRelevante+1);
			//this.r1 =  rel.docList.size() / (ultimoRelevante+1);
			return this.p10;
			
			
		}
		private void printQuery() {
			System.out.println("\nAnalizando la Query: " + id);
			if (this.qText_sinModificar != "")
				System.out.println("Texto Query SIN MODIFICAR : " + qText_sinModificar);
			System.out.println("Texto Query : " + qText);

			if (rel != null) {
				if (p10 != -1)
					System.out.println("P@10: " + p10);
				if (p20 != 1)
					System.out.println("P@20: " + p20);
				if (r10 != -1)
					System.out.println("P@r10: " + r10);
				if (r20 != -1)
					System.out.println("P@r20: " + r20);
				if (map != -1)
					System.out.println("MAP: " + map);
				if (r1 != -1)
					System.out.println("R1: " + r1);


			}

			if (this.explicacion != null) {
				System.out.println("\nExplicación de la query expandida: \n");
				for (String exp : explicacion) {
					System.out.println(exp);
				}
			}

		}

		public void NResults(IndexSearcher searcher, Parametros parametros) {

			System.out.println("\nMostrando top " + parametros.topN + " resultados\n");
			Document doc = null;
			ScoreDoc[] hits = this.results.scoreDocs;
			int numTotalHits = this.results.totalHits;
			int docs = parametros.topN;
			ArrayList<String> listVisual = new ArrayList<String>(Arrays.asList(parametros.visual.split(",")));

			if (docs > numTotalHits) {
				System.out.println("El numero topN es mayor que el numero de documentos:  " + numTotalHits + " docs");
				docs = numTotalHits;
			}

			for (int i = 0; i < docs; i++) {
				int score = i + 1;
				try {
					doc = searcher.doc(hits[i].doc);
				} catch (IOException e) {

					e.printStackTrace();
				}

				String docId = doc.get("docid");
				System.out.print("\t Score[" + score + "]:");

				if (rel.esRelevante(Integer.parseInt(docId))) {
					System.out.print(" (R) ");
				} else
					System.out.print("     ");

				System.out.print(" " + hits[i].score);

				if (listVisual.contains("I"))
					System.out.print(" [DocID]: " + docId);

				if (listVisual.contains("T"))
					System.out.print(" [Title]: " + doc.get("title"));

				if (listVisual.contains("W"))
					System.out.print(" [Abstracts]: " + doc.get("abstract"));

				if (listVisual.contains("A"))
					System.out.print(" [Authors]: " + doc.get("authors"));

				if (listVisual.contains("B"))
					System.out.print(" [Authors]: " + doc.get("date"));

				System.out.print("\n");
			}

		}

		public void procesarQuery(IndexSearcher searcher, Analyzer analyzer, Parametros parametros) {

			int minResults = Math.max(parametros.topN, 20);
			ArrayList<String> queryList = new ArrayList<String>();

			for (String s : parametros.fieldList) {
				queryList.add(QueryParser.escape(this.qText));
				// System.out.println("ASASASASA " + this.qText);

			}
			try {
				query = MultiFieldQueryParser.parse(queryList.toArray(new String[queryList.size()]),
						parametros.getFieldL().toArray(new String[parametros.getFieldL().size()]), analyzer);

				// System.out.println(query);
				this.results = searcher.search(query, minResults);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	/**
	 * Parser de queries.text
	 **/
	private class CRANQueryParser {
		private BufferedReader reader;
		private ArrayList<CRANQuery> queries;

		public CRANQueryParser(String queriesfile) throws FileNotFoundException {
			this.reader = new BufferedReader(new FileReader(new File(queriesfile)));
			this.queries = new ArrayList<CRANQuery>();
		}

		public ArrayList<CRANQuery> parse() throws IOException {
			CRANQuery query = null;
			String line = "";
			char estado = 0;
			int queryIndex = 1;
			while ((line = reader.readLine()) != null) {
				if ((line = line.trim()).isEmpty()) {
					continue;
				}
				if (line.charAt(0) == '.') {
					estado = line.charAt(1);
					if (estado == 'I') {
						if (query != null) {
							queries.add(query);
						}
						query = new CRANQuery();
						// No nos interesa guardar el I del fichero de queries
						// debido a que no lo usamos
						// query.id =
						// Integer.parseInt(line.substring(2).trim());
						query.id = queryIndex++;

					}
				} else {
					switch (estado) {
					case 'W':
						query.addText(line);
						break;
					case 'I':
						break;
					default:
					}
				}
			}
			queries.add(query);
			reader.close();
			return queries;
		}
	}

	/**
	 * Clase para guardar los documentos relevantes para cada query *
	 */
	private class QRel {
		private int qid;
		private ArrayList<Integer> docList; // lista con los ids de los
											// documentos relevantes

		public QRel() {
			qid = -1;
			docList = new ArrayList<Integer>();
		}

		public int getQid() {
			return qid;
		}

		public void setQid(int qid) {
			this.qid = qid;
		}

		public ArrayList<Integer> getDocList() {
			return docList;
		}

		public void setDid(ArrayList<Integer> didList) {
			this.docList = didList;
		}

		// Anade un nuevo documentId relevante para esta query

		public void addDid(String newdid) {
			docList.add(Integer.parseInt(newdid));
		}

		// Comprueba si el docId esta entre los relevantes para la query

		public boolean esRelevante(int docId) {
			return docList.contains(docId);
		}

		// Metodo que devuelve el numero de relevantes de una query
		public int numRels() {
			return this.docList.size();
		}

		@Override
		public String toString() {
			return "QRelId: " + this.qid + " | DidList(" + docList.size() + "): " + docList;
		}
	}

	/**
	 * Clase para parsear qrels.text
	 * 
	 */
	private class QRelParser {
		private BufferedReader reader;
		private ArrayList<QRel> qrels;

		public QRelParser(String queriesfile) throws FileNotFoundException {
			this.reader = new BufferedReader(new FileReader(new File(queriesfile)));
			this.qrels = new ArrayList<QRel>();
		}

		public ArrayList<QRel> parse() throws IOException {
			String line = "";
			int lastId = 0;
			QRel lastQRel = null;
			while ((line = reader.readLine()) != null) {
				if ((line = line.trim()).isEmpty()) {
					continue;
				}
				String[] tokens = line.split("\\s+");
				if (lastId != Integer.parseInt(tokens[0])) {
					if (lastQRel != null)
						qrels.add(lastQRel);
					lastQRel = new QRel();
					lastQRel.setQid(Integer.parseInt(tokens[0]));
					lastQRel.addDid(tokens[1]);
					lastId = Integer.parseInt(tokens[0]);
				} else {
					lastQRel.addDid(tokens[1]);
				}
			}
			qrels.add(lastQRel);
			reader.close();
			return qrels;
		}

	}

	class Parametros {
		// parametros indexer
		private String mode = "";
		private String indexPath = "";
		private String collPath = "";
		private String indexingMode = "";
		private String searchMode ="";
		private float lambdaOrMu = -1f;

		public List<Integer> limitList;
		String indexinPath;
		int topN, cutN;
		List<Integer> numberList;
		List<String> queryList;
		List<String> fieldList;
		String visual;
		ClaseConsulta type;
		boolean explain;

		public Parametros() {

			indexinPath = null;
			fieldList = null;
			topN = -1;
			cutN = -1;
			visual = "I,T";
			type = null;
			numberList = new ArrayList<>();
			numberList.add(-1);
			numberList.add(-1);
			explain = false;

		}

		public List<String> getFieldL() {

			return fieldList;
		}

		
		public float getLambdaOrMu() {
			return lambdaOrMu;
		}

		public void setLambdaOrMu(float lambdaOrMu) {
			this.lambdaOrMu = lambdaOrMu;
		}

		public Integer getEndN() {
			return numberList.get(1);

		}

		public Integer getStartN() {
			return numberList.get(0);

		}
		

		public String getMode() {
			return mode;
		}

		public void setMode(String mode) {
			this.mode = mode;
		}

		public String getIndexPath() {
			return indexPath;
		}

		public void setIndexPath(String indexPath) {
			this.indexPath = indexPath;
		}

		public String getCollPath() {
			return collPath;
		}

		public void setCollPath(String collPath) {
			this.collPath = collPath;
		}

		public String getIndexingMode() {
			return indexingMode;
		}
		public String getSearchMode(){
			return this.searchMode;
		}

		public void setIndexingMode(String indexingMode) {
			this.indexingMode = indexingMode;
		}

		public void parseParams(String[] paramList) {
			for (int i = 0; i < paramList.length; i++) {

				if (null != paramList[i]) {

					switch (paramList[i]) {

					case "-openmode":
						mode = paramList[i + 1];
						if (!(mode.contains("create") || mode.contains("append")
								|| mode.contains("created_or_append"))) {
							System.err.println(
									"parametro openmode faltante o erroneo. FORMATO: -openmode [created|append|created_or_append]");
							System.exit(1);
						}
						i++;
						break;
					case "-index":
						indexPath = paramList[i + 1];
						i++;
						System.out.println("\tDirectorio del index: " + indexPath);
						break;
					case "-coll":
						collPath = paramList[i + 1] + "cran.all.1400";
						i++;
						System.out.println("\tDirectorio de los ficheros CRAN: " + collPath);
						break;
						
					case "-indexingmode":
						indexingMode = paramList[i + 1];
						if (!(indexingMode.contains("default") || indexingMode.contains("jm")
								|| indexingMode.contains("dir"))) {
							System.err.println(
									"parametro openmode faltante o erroneo. FORMATO: -openmode [created|append|created_or_append]");
							System.exit(1);
						}
						if (indexingMode.contains("jm") || indexingMode.contains("dir")) {
							lambdaOrMu = Float.valueOf(paramList[i + 2]);
							i++;
						}
						i++;
						break;

					case "-indexin":
						indexinPath = paramList[i + 1];
						i++;
						break;

					case "-cut":
						cutN = Integer.parseInt(paramList[i + 1]);
						i++;
						break;

					case "-top":
						topN = Integer.parseInt(paramList[i + 1]);
						i++;
						break;

					case "-queries":
						type = ClaseConsulta.QUERIES;
						int j = 0;
						if (!paramList[i + 1].equals("all")) {

							while ((i + 1 < paramList.length) && (!paramList[i + 1].startsWith("-"))) {
								numberList.set(j, Integer.parseInt(paramList[i + 1]));
								i++;
								j++;
							}

						}
						break;

					case "-fieldsproc":
						fieldList = new ArrayList<>();
						i++;
						String[] fieldsArray = paramList[i].split(",");
						for (String s : fieldsArray) {
							if (s.equals("T")) {
								fieldList.add("title");

							}
							if (s.equals("W")) {
								fieldList.add("abstract");

							}
						}
						break;

					case "-fieldsvisual":
						visual = paramList[i + 1];
						i++;

						break;

					case "-rf1":
						type = ClaseConsulta.RF1;
						limitList = new ArrayList<>();
						limitList.add(Integer.parseInt(paramList[i + 1]));
						i++;
						limitList.add(Integer.parseInt(paramList[i + 1]));
						i++;
						limitList.add(Integer.parseInt(paramList[i + 1]));
						i++;
						break;

					case "-rf2":
						type = ClaseConsulta.RF2;
						limitList = new ArrayList<>();
						limitList.add(Integer.parseInt(paramList[i + 1]));
						i++;
						break;

					case "-explain":
						explain = true;
						break;
					case "-search":
						searchMode = paramList[i + 1];
						if (!(searchMode.contains("default") ||searchMode.contains("jm")
								|| searchMode.contains("dir"))) {
							System.err.println(
									"parametro search faltante..");
							System.exit(1);
						}
						i++;
						break;

					}
				}
			}

		}

	}

	private void managerQuery(IndexSearcher searcher, Analyzer analyzer, Parametros parametros,
			ArrayList<CRANQuery> queries) {

		for (CRANQuery q : queries) {

			if (q.getRel() != null) {

				if (parametros.getEndN() != -1) {
					if ((q.getId() >= parametros.getStartN()) && (q.getId() <= parametros.getEndN())) {
						q.procesarQuery(searcher, analyzer, parametros);
						q.proMetricas(searcher, parametros, this);
						q.printQuery();
						if (parametros.topN != -1)
							q.NResults(searcher, parametros);
					}
				} else if (parametros.getStartN() != -1) {
					if (q.getId() == parametros.getStartN()) {
						q.procesarQuery(searcher, analyzer, parametros);
						q.proMetricas(searcher, parametros, this);
						q.printQuery();
						if (parametros.topN != -1)
							q.NResults(searcher, parametros);
					}
				}

				else {
					q.procesarQuery(searcher, analyzer, parametros);
					q.proMetricas(searcher, parametros, this);
					q.printQuery();
					if (parametros.topN != -1)
						q.NResults(searcher, parametros);
				}

			}
		}

	}

	// ------------- RF'S -------------

	public void procesarRF1(IndexSearcher searcher, Analyzer analyzer, DirectoryReader reader, Parametros parametros,
			ArrayList<CRANQuery> queries) throws ParseException, IOException {

		QRel docRelevantes;
		CRANRelevance rel = new CRANRelevance();
		ArrayList<String> queriesModificadas = new ArrayList<>();

		// inicializo todo
		this.clearEval();

		// variables de control
		int qStart = 0, qEnd = 0;

		/* Inicializo variables para ver que query me estan pidiendo */

		for (CRANQuery q : queries)
			qEnd++;

		if ((parametros.getStartN() != -1)) {
			if (parametros.getStartN() != -1) {
				qStart = parametros.getStartN() - 1;
				if (parametros.getEndN() != -1)
					qEnd = parametros.getEndN();
				else
					qEnd = qStart;
			}

		}

		int qSaveStart = qStart;
		// cada query
		while (qStart <= qEnd) {

			// nuevos terminos
			ArrayList<String> terminosDoc = new ArrayList<>();
			ArrayList<String> explicacion = new ArrayList<>();

			// resultados donde se muestran los mejores!!
			ArrayList<CRANRelevance.ResultTFIDF> resultadosTFIDF = new ArrayList<>();
			ArrayList<CRANRelevance.ResultIDF> resultadosIDF = new ArrayList<>();

			// calculo las metricas segun el field
			ArrayList<CRANRelevance.ResultTFIDF> resultadosTFIDFTitle = null;
			ArrayList<CRANRelevance.ResultTFIDF> resultadosTFIDFAbstract = null;
			ArrayList<CRANRelevance.ResultIDF> resultadosIDFTitle = null;
			ArrayList<CRANRelevance.ResultIDF> resultadosIDFAbstract = null;

			CRANQuery q = queries.get(qStart);

			// lista documentos relevantes
			docRelevantes = q.getRel();
			if (null == docRelevantes) {
				docRelevantes = new QRel();
				docRelevantes.docList = new ArrayList<>();
			}

			// las queries pueden ir sobre uno o dos terminos
			for (String field : parametros.getFieldL()) {

				// inicializo las listas de resultados
				if (field.equals("abstract")) {

					if (resultadosTFIDFAbstract == null) {
						DirectoryReader reader2 = DirectoryReader
								.open(FSDirectory.open(Paths.get(parametros.indexinPath)));
						resultadosTFIDFAbstract = rel.processIDFTF(reader2, field);
						// System.out.print("INICIALIZO 1 ");

						// La acabo de inicializar asi que los meto
						resultadosTFIDF.addAll(resultadosTFIDFAbstract);
					}

					if (resultadosIDFAbstract == null) {

						resultadosIDFAbstract = rel.processIDFQuery(reader, q.getqText(), field,
								parametros.limitList.get(0));
						// System.out.print("INICIALIZO 1 ");

						// La acabo de inicializar asi que los meto
						resultadosIDF.addAll(resultadosIDFAbstract);
					}

				}

				if (field.equals("title")) {

					if (resultadosTFIDFTitle == null) {
						DirectoryReader reader2 = DirectoryReader
								.open(FSDirectory.open(Paths.get(parametros.indexinPath)));
						resultadosTFIDFTitle = rel.processIDFTF(reader2, field);
						// System.out.print("INICIALIZO 2 ");

						resultadosTFIDF.addAll(resultadosTFIDFTitle);
					}

					if (resultadosIDFTitle == null) {

						resultadosIDFTitle = rel.processIDFQuery(reader, q.getqText(), field,
								parametros.limitList.get(0));

						// System.out.print("INICIALIZO 2 ");
						resultadosIDF.addAll(resultadosIDFTitle);
					}

				}

			}
			// ordeno las colecciones
			Collections.sort(resultadosIDF);
			Collections.sort(resultadosTFIDF);

			for (int contador = 0; contador < parametros.limitList.get(0); contador++) {

				if (parametros.explain) {
					// System.out.println("\tResultIDF= " +
					// resultadosIDF.get(contador));
					String exp = new String("\tResultIDF= " + resultadosIDF.get(contador));
					explicacion.add(exp);
				}

				String termino = resultadosIDF.get(contador).getTermino();
				terminosDoc.add(termino);

			}

			int numeroDocumentos = Math.min(parametros.limitList.get(2), docRelevantes.docList.size());
			terminosDoc.addAll(rel.mejoresTermsDoc(resultadosTFIDF, docRelevantes.docList, numeroDocumentos,
					parametros.limitList.get(1), parametros.explain, explicacion));

			String queryModificada = new String();

			for (String s : terminosDoc) {
				queryModificada += s + " ";
			}

			// System.out.println("\n");
			// System.out.println("\tQuery sin modificar : " + q.getqText());
			// System.out.println("\tQuery ya modificada : " + queryModificada);

			q.setqRext_sinModificar(q.qText);
			q.setExplicacion(explicacion);

			queriesModificadas.add(queryModificada);

			if (parametros.explain)
				q.setExplicacion(explicacion);

			qStart++;
			if (qStart + 1 > qEnd)
				break;
		}

		// cambio todas las queries originales por las modificadas

		int qIndex = 0;
		while (qSaveStart <= qEnd) {

			if (qIndex == queriesModificadas.size())
				break;
			CRANQuery q = queries.get(qSaveStart);
			q.setqRext(queriesModificadas.get(qIndex));

			qIndex++;
			qSaveStart++;
			if (qSaveStart + 1 > qEnd)
				break;

		}
		System.out.println("\n------------------------------------------------------------------\n");
		System.out.println("\tRF1  - QUERIES EXPANDIDAS");
		System.out.println("\n------------------------------------------------------------------\n");
		managerQuery(searcher, analyzer, parametros, queries);
		showResults();

	}

	public void procesarRF2(IndexSearcher searcher, Analyzer analyzer, Parametros parametros,
			ArrayList<CRANQuery> queries) {

		QRel docRelevantes;
		CRANRelevance rel = new CRANRelevance();
		ArrayList<String> queriesModificadas = new ArrayList<>();

		// inicializo todo
		this.clearEval();

		// variables de control
		int qStart = 0, qEnd = 0;

		/* Inicializo variables para ver que query me estan pidiendo */

		for (CRANQuery q : queries)
			qEnd++;

		if ((parametros.getStartN() != -1)) {
			if (parametros.getStartN() != -1) {
				qStart = parametros.getStartN() - 1;
				if (parametros.getEndN() != -1)
					qEnd = parametros.getEndN();
				else
					qEnd = qStart;
			}

		}

		// cada query
		int qSaveStart = qStart;

		while (qStart <= qEnd) {
			CRANQuery q = queries.get(qStart);
			String query = q.getqText();

			// lista documentos relevantes
			docRelevantes = q.getRel();

			if (null == docRelevantes) {
				docRelevantes = new QRel();
				docRelevantes.docList = new ArrayList<>();
			}

			int numeroDocumentos = Math.min(parametros.limitList.get(0), docRelevantes.docList.size());

			// concateno todas las queries de los dicumentos
			for (int k = 0; k < numeroDocumentos; k++) {

				String titulo = null;

				try {
					titulo = searcher.doc(docRelevantes.docList.get(k) - 1).get("title").toString();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// System.out.println(k + "titulo" + titulo);
				query += titulo;

			}

			queriesModificadas.add(query);

			qStart++;
			if (qStart + 1 > qEnd)
				break;
		}

		// cambio todas las queries originales por las modificadas
		int qIndex = 0;
		while (qSaveStart <= qEnd) {

			if (qIndex == queriesModificadas.size())
				break;
			CRANQuery q = queries.get(qSaveStart);
			q.setqRext(queriesModificadas.get(qIndex));
			System.out.println("1111111111111" + q.getqText());

			qIndex++;
			qSaveStart++;
			if (qSaveStart + 1 > qEnd)
				break;

		}
		managerQuery(searcher, analyzer, parametros, queries);
		showResults();

	}

	public static void main(String argv[]) {

		System.out.println("CRANFIELD INDEXER\\EVAL - RI 2017.\n");

		// indexar

		// ParseaParametros
		CRANEval eval = new CRANEval();
		CRANIndexer index = new CRANIndexer();

		
		Parametros param = eval.new Parametros();
		param.parseParams(argv);
		index.indexDocuments(param);
		

		// ParseaQuerys
		ArrayList<CRANQuery> queries = null;
		ArrayList<QRel> rels = null;
		CRANQueryParser qparser = null;
		QRelParser rparser = null;

		// lucenes

		DirectoryReader reader = null;
		IndexSearcher searcher = null;
		Analyzer analyzer = null;

		try {
			qparser = eval.new CRANQueryParser("docs/cran.qry");
			rparser = eval.new QRelParser("docs/cranqrel");

		} catch (Exception e) {

			e.printStackTrace();
		}

		// caso en el que solo queramos indexar
		if (param.indexinPath == null)
			return;
		
		
		try {
			reader = DirectoryReader.open(FSDirectory.open(Paths.get(param.indexinPath)));
		} catch (IOException e) {

			e.printStackTrace();
		}

		searcher = new IndexSearcher(reader);
		analyzer = new StandardAnalyzer();

		if (!param.getSearchMode().equals("")) {

			switch (param.getSearchMode()) {

			case "jm":
				System.out.println("ENTROOOOO: JM-search " );
				LMJelinekMercerSimilarity jmSimilarity = new LMJelinekMercerSimilarity(param.getLambdaOrMu());
				searcher.setSimilarity(jmSimilarity);
				break;
			case "dir":
				LMDirichletSimilarity dirSimilarity = new LMDirichletSimilarity(param.getLambdaOrMu());
				searcher.setSimilarity(dirSimilarity);
				break;
			default:
				System.out.println("ENTROOOOO: JM-search " );
				break;
			}
		}

		try {
			queries = qparser.parse();
			rels = rparser.parse();

		} catch (IOException e) {

			e.printStackTrace();
		}

		/* Se asocian queries con los documentos relevantes */

		for (CRANQuery q : queries) {
			for (QRel qrel : rels) {
				if (qrel.getQid() == q.getId()) {
					q.setQrel(qrel);
					rels.remove(qrel);
					break;
				}
			}
		}

		/* Se realiza una funcion o otra */
		switch (param.type) {

		case QUERIES:
			eval.managerQuery(searcher, analyzer, param, queries);
			eval.showResults();
			break;

		case RF1:
			eval.managerQuery(searcher, analyzer, param, queries);
			eval.showResults();
			try {
				eval.procesarRF1(searcher, analyzer, reader, param, queries);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case RF2:
			eval.managerQuery(searcher, analyzer, param, queries);
			eval.showResults();
			eval.procesarRF2(searcher, analyzer, param, queries);
			break;

		}

	}
}
